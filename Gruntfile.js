module.exports = function (grunt) {
    var watchFiles = ['Gruntfile.js', 'server/**/*.js', 'public/**/*.js'];

    grunt.initConfig({
        jshint: {
            files: {
                src: watchFiles
            }
        },
        jasmine_node: {
            specNameMatcher: "_spec",
            projectRoot: ".",
            requirejs: false,
            forceExit: true,
            jUnit: {
                report: false,
                savePath: "./build/reports/jasmine/",
                useDotNotation: true,
                consolidate: true
            }
        },
        watch: {
            scripts: {
                files: watchFiles,
                tasks: ['jshint', 'jasmine_node'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-jasmine-node');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['jshint','jasmine_node']);
};