Dice Game
=========

Windows:

install git bash: http://git-scm.com/download/win

clone repo: $>https://<username>:<password>@bitbucket.org/MechHattan/dice-game.git

install node: http://nodejs.org/

install bower: $>npm install -g bower

install grunt-cli: $>npm install -g grunt-cli

cd to cloned repo and execute: $>npm install

run grunt to verify installation: $>grunt

start node: $>npm start

verify game in browser: http://localhost:9876

