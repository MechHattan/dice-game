(function (window, document) {
    'use strict';

    window.name = "NG_DEFER_BOOTSTRAP!";

    var head = document.getElementsByTagName('head')[0];

    loadConfig(scripts()[scripts().length - 1].getAttribute('data-config'));

    function scripts() {
        return document.getElementsByTagName('script');
    }

    function loadConfig(url) {
        var xhr = new window.XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.onload = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var urls = JSON.parse(xhr.responseText);

                urls.forEach(function(url) {
                    var script = document.createElement('script');
                    script.src = url;
                    script.async = false;
                    head.appendChild(script);
                });
            }
        };
        xhr.onerror = function () {
            console.error(xhr.statusText);
        };
        xhr.send();
    }
})(this, this.document);