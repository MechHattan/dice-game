(function() {
    'use strict';

    var app = angular.module('app');

    app.factory('AccountsResource', ['$resource', function ($resource) {
        return {
            signup: function () {
                return new $resource('/service/accounts/signup');
            },
            signin: function () {
                return new $resource('/service/accounts/signin');
            },
            signout: function () {
                return new $resource('/service/accounts/signout');
            },
            currentPlayer: function () {
                return new $resource('/service/accounts/current');
            }
        };
    }]);

    app.factory('CurrentPlayerService', ['AccountsResource', '$location', function (AccountsResource, $location) {
        var playerService = {};
        playerService.currentPlayer = {};

        playerService.getUsername = function () {
            return playerService.currentPlayer.username;
        };

        playerService.signout = function () {
            AccountsResource.signout().save({}, function () {
                playerService.currentPlayer = {};
                $location.path('/');
            });
        };

        playerService.signup = function () {
            simulateInputValuesToFixBugInChromeAutofill();
            if (this.signupForm.$valid) {
                var player = AccountsResource.signup().save({
                    username: this.username,
                    firstName: this.firstName,
                    lastName: this.lastName,
                    email: this.email,
                    password: this.password1
                }, function () {
                    playerService.currentPlayer = player;
                    $location.path('/');
                });
            }
        };

        playerService.signin = function () {
            simulateInputValuesToFixBugInChromeAutofill();
            if (this.signinForm.$valid) {
                var account = AccountsResource.signin().save(this.account, function () {
                    playerService.currentPlayer = account;
                    $location.path('/');
                });
            }
        };

        AccountsResource.currentPlayer().get({}, function (playerData) {
            playerService.currentPlayer = playerData;
        });

        return playerService;
    }]);

    app.controller('SignInCtrl', ['$scope', 'CurrentPlayerService', function ($scope, CurrentPlayerService) {
        $scope.signin = CurrentPlayerService.signin;
    }]);

    app.controller('SignupCtrl', ['$scope', 'CurrentPlayerService', function ($scope, CurrentPlayerService) {
        $scope.signup = CurrentPlayerService.signup;
    }]);

    app.controller('AccountCtrl', ['$scope', 'CurrentPlayerService', function ($scope, CurrentPlayerService) {
        $scope.getUsername = CurrentPlayerService.getUsername;
        $scope.signout = CurrentPlayerService.signout;
    }]);

    function simulateInputValuesToFixBugInChromeAutofill() {
        $('#accountform input,#accountform select').each(function () {
            $(this).trigger('input');
        });
    }
})(this.angular);