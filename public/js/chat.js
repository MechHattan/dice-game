(function(angular) {
    'use strict';

    angular.module('app').controller('ChatCtrl', ['$scope', '$resource', 'messaging', function($scope, $resource, messaging) {
        var Message = new $resource('/service/lobbies/main');

        $scope.messages = [];

        messaging.on('chat', function(event) {
            $scope.messages.push(event);
            $scope.$apply();
        });

        $scope.send = function() {
            if($scope.message.text) {
                new Message($scope.message).$save();
                $scope.message.text = '';
            }
        };
    }]);
})(this.angular);