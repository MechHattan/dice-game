(function(angular) {
    'use strict';

    angular.module('app').directive('passwordCheck', [function($document) {
        return {
            require: 'ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var firstPasswordId = "#" + attrs.passwordCheck;
                elem.add(firstPasswordId).on('blur', function() {
                    scope.$apply(function() {
                        var firstPassword = $(firstPasswordId).val();
                        var secondPassword = elem.val();

                        var passwordsMatch = firstPassword === secondPassword;
                        var passwordsNotEmpty = (firstPassword !== "" && secondPassword !== "");
                        if(!passwordsMatch && passwordsNotEmpty) {
                            ctrl.$setValidity('passwordsMatch', false);
                        } else {
                            ctrl.$setValidity('passwordsMatch', true);
                        }

                        if(firstPassword.length < 6) {
                            ctrl.$setValidity('passwordsTooShort', false);
                        } else {
                            ctrl.$setValidity('passwordsTooShort', true);
                        }
                    });
                });
            }
        };
    }]);
})(this.angular);