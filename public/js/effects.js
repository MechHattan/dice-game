(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('EffectsResource', ['$resource', function($resource) {
        return $resource('/service/effects/:id');
    }]);

    app.factory('EffectsService', ['EffectsResource', function(EffectsResource) {
        var effectsService = {};
        effectsService.effects = [];
        effectsService.effect = {};

        effectsService.findall = function() {
            effectsService.effects = EffectsResource.query();
        };

        effectsService.findone = function(effectId) {
            effectsService.effect = EffectsResource.get({id:effectId});
        };

        effectsService.save = function() {
            new EffectsResource(this.effect).$save(function(effect) {
                effectsService.effect = {};
                effectsService.findall();
                alert(effect.name + ' created successfully.');
            });
        };

        effectsService.findall();
        return effectsService;
    }]);

    app.controller('EffectCtrl', ['$scope', '$routeParams', 'EffectsService', function($scope, $routeParams, EffectsService) {
        $scope.effectsService = EffectsService;
        $scope.save = EffectsService.save;

        if($routeParams.id) {
            EffectsService.findone($routeParams.id);
        }
    }]);

    app.controller('EffectsCtrl', ['$scope', 'EffectsService', function($scope, EffectsService) {
        $scope.effectsService = EffectsService;
    }]);
})(this.angular);