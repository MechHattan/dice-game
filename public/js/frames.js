(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('Frame', ['$resource', function($resource) {
        return $resource('/service/frames/:id');
    }]);

    app.controller('FrameCtrl', ['$scope', '$routeParams', '$rootScope', 'Frame', function($scope, $routeParams, $rootScope, Frame) {
        if($routeParams.id) {
            $scope.frame = Frame.get({id:$routeParams.id});
        } else {
            $scope.frame = {modules:[]};
        }

        $scope.save = function() {
            new Frame(this.frame).$save(function(frame) {
                $scope.frame = {modules:[]};
                $rootScope.$broadcast('Database/FrameUpdated');
                alert(frame.name + ' created successfully.');
            });
        };

        $scope.addModule = function() {
            $scope.frame.modules.push({});
        };
    }]);

    app.controller('FramesCtrl', ['$scope', '$rootScope', 'Frame', function($scope, $rootScope, Frame) {
        function updateFrames() {
            $scope.frames = Frame.query();
        }

        updateFrames();

        $rootScope.$on('Database/FrameUpdated', function() {
            updateFrames();
        });
    }]);
})(this.angular);