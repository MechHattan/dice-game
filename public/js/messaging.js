(function(angular) {
    'use strict';

    var app = angular.module('app');

    var eventSubscribers = {

    };

    if (!!window.EventSource) {
        var source = new EventSource('/service/messages');

        source.addEventListener('message', function (e) {
            var event = JSON.parse(e.data);
            if(event.type) {
                var subscribers = eventSubscribers[event.type] || [];
                subscribers.forEach(function(callback) {
                   callback.call(null, event);
                });
            }
        }, false);

    } else {
        alert("Your browser is not supported (no server sent event support)");
    }

    app.factory('messaging', [function() {
        return     {
            on: function(type, callback) {
                var subscribers = eventSubscribers[type];
                if(!subscribers) {
                    subscribers = eventSubscribers[type] = [];
                }
                subscribers.push(callback);
            }
        };
    }]);
})(this.angular);