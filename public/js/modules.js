(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('Module', ['$resource', function($resource) {
        return new $resource('/service/modules/:id');
    }]);

    app.controller('ModuleCtrl', ['$scope', '$routeParams', '$rootScope', 'Module', function($scope, $routeParams, $rootScope, Module) {
        if($routeParams.id) {
            $scope.module = Module.get({id:$routeParams.id});
        }

        $scope.save = function() {
            new Module(this.module).$save(function(module) {
                $scope.module = {};
                $rootScope.$broadcast('Database/ModuleUpdated');
                alert(module.name + ' created successfully.');
            });
        };
    }]);

    app.controller('ModulesCtrl', ['$scope', '$rootScope', 'Module', function($scope, $rootScope, Module) {
        function updateModules() {
            $scope.modules = Module.query();
        }

        updateModules();

        $rootScope.$on('Database/ModuleUpdated', function() {
            updateModules();
        });
    }]);
})(this.angular);