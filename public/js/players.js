(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('Player', ['$resource', function($resource) {
        return new $resource('/service/players/:id');
    }]);

    app.controller('PlayerCtrl', ['$scope', '$routeParams', 'Player', function($scope, $routeParams, Player) {
        $scope.player = Player.get({id:$routeParams.id});
    }]);

    app.controller('PlayersCtrl', ['$scope', '$routeParams', 'Player', function($scope, $routeParams, Player) {
        $scope.players = Player.query();
    }]);

    app.controller('OnlinePlayersCtrl', ['$scope', '$routeParams', 'Player', function($scope, $routeParams, Player) {
        $scope.players = [];
    }]);
})(this.angular);