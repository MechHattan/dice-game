xdescribe('SignInCtrl', function() {
    beforeEach(module('app'));

    var ctrl, scope;
    var currentPlayerService = {
        signin: jasmine.createSpy()
    };
    beforeEach(inject(function($controller, $rootScope) {
        scope = $rootScope.new();
        ctrl = $controller('SignInCtrl', {
            $scope: scope,
            CurrentPlayerService: currentPlayerService
        });
    }));

    it('should assign signin to currentPlayerService.signin', function() {
        expect(scope.signin).toBe(currentPlayerService.signin);
    });
});