(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('Utility', ['$resource', function($resource) {
        return new $resource('/service/utilities/:id');
    }]);

    app.controller('UtilityCtrl', ['$scope', '$routeParams', '$rootScope', 'Utility', function($scope, $routeParams, $rootScope, Utility) {
        if($routeParams.id) {
            $scope.utility = Utility.get({id:$routeParams.id});
        }

        $scope.save = function() {
            new Utility(this.utility).$save(function(utility) {
                $scope.utility = {};
                $rootScope.$broadcast('Database/UtilityUpdated');
                alert(utility.name + ' created successfully.');
            });
        };
    }]);

    app.controller('UtilitiesCtrl', ['$scope', '$rootScope', 'Utility', function($scope, $rootScope, Utility) {
        function updateUtilities() {
            $scope.utilities = Utility.query();
        }

        updateUtilities();

        $rootScope.$on('Database/UtilityUpdated', function() {
            updateUtilities();
        });
    }]);
})(this.angular);