(function(angular) {
    'use strict';

    var app = angular.module('app');

    app.factory('Weapon', ['$resource', function($resource) {
        return new $resource('/service/weapons/:id');
    }]);

    app.controller('WeaponCtrl', ['$scope', '$routeParams', '$rootScope', 'Weapon', function($scope, $routeParams, $rootScope, Weapon) {
        if($routeParams.id) {
            $scope.weapon = Weapon.get({id:$routeParams.id});
        }

        $scope.save = function() {
            new Weapon(this.weapon).$save(function(weapon) {
                $scope.weapon = {};
                $rootScope.$broadcast('Database/WeaponUpdated');
                alert(weapon.name + ' created successfully.');
            });
        };
    }]);

    app.controller('WeaponsCtrl', ['$scope', '$rootScope', 'Weapon', function($scope, $rootScope, Weapon) {
        function updateWeapons() {
            $scope.weapons = Weapon.query();
        }

        updateWeapons();

        $rootScope.$on('Database/WeaponUpdated', function() {
            updateWeapons();
        });
    }]);
})(this.angular);