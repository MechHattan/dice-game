(function(angular) {
    function configRoutes(routes, defaultRoute) {
        angular.module('app').config(['$routeProvider', function ($routeProvider) {
            routes.forEach(function(route) {
                $routeProvider.when(route, {
                    templateUrl: '/html' + route.replace(':','') + '.html'
                });
            });

            $routeProvider.otherwise({redirectTo: defaultRoute});
        }]);
    }

    configRoutes(['/accounts/signin',
        '/accounts/signup',
        '/create/effect',
        '/create/frame',
        '/create/module',
        '/create/utility',
        '/create/weapon',
        '/database/effects',
        '/database/effects/:id',
        '/database/frames',
        '/database/frames/:id',
        '/database/modules',
        '/database/modules/:id',
        '/database/players',
        '/database/players/:id',
        '/database/utilities',
        '/database/utilities/:id',
        '/database/weapons',
        '/database/weapons/:id',
        '/welcome'],
    '/welcome');
})(this.angular);