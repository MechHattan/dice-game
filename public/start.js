(function(angular) {
    'use strict';

    angular.element(document).ready(function() {
        angular.bootstrap(document, [angular.module('app').name]);
    });
})(this.angular);