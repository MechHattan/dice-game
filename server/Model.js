var FileDB = require('./FileDB'),
    _ = require('underscore');

module.exports = require('./util/extend')({
    constructor: function (attributes) {
        _.extend(this, attributes);
    },
    save: function () {
        var Constructor = Object.getPrototypeOf(this).constructor;
        return Constructor.db().save(this);
    },
    clean: function () {
        return this;
    }
}, {
    all: function () {
        return this.db().all();
    },
    findOne: function (query) {
        return this.db().findOne(query);
    },
    db: function () {
        if (!this.dbInst) {
            this.dbInst = new FileDB(process.cwd() + '/db/' + this.collection, this);
        }
        return this.dbInst;
    },
    servicify: function (app) {
        var _this = this;

        app.get('/service/' + this.collection, function (req, res) {
            res.json(cleanAll(_this.all()));
        });

        app.get('/service/' + this.collection + '/:' + key(this), function (req, res) {
            query = {};
            query[key(_this)] = req.params[key(_this)];
            var model = _this.findOne(query);
            res.json(model ? model.clean() : {});
        });

        app.post('/service/' + this.collection, function (req, res) {
            res.json(new _this(req.body).save().clean());
        });
    }
});

function cleanAll(models) {
    var results = [];
    if (models) {
        models.forEach(function (model) {
            results.push(model.clean());
        });
    }
    return results;
}

function key(Model) {
    return Model.key || 'id';
}