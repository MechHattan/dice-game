var express = require('express'),
    app = express(),
    cli = require('cli'),
    options = cli.parse();

app.set('port', options.port? options.port : 9876);

// handle json body data (req.body)
app.use(express.json());

// simple session support (req.session)
app.use(express.cookieParser());
app.use(express.session({secret: 'ourprivatekey'}));

// server router
app.use(app.router);

// static resources
app.use('/components', express.static(__dirname + '/../bower_components'));
app.use(express.static(__dirname + '/../public'));

app.loadRoutes = function(pattern) {
    require('./util/requireAll')(pattern).forEach(function(module) {
        module.call(null, app);
    });
};

app.loadRoutes('server/routes/*.js');

app.listen(app.get('port'), function(){
    console.log('MechHatton server listening on port ' + app.get('port'));
});