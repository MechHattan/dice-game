var _ = require('underscore');

module.exports = require('../Model').extend({
    clean: function() {
        return _.omit(this, 'passhash');
    }
}, {
    collection: 'players'
});