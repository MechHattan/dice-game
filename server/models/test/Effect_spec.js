var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Effect', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Effect = proxyQuire('../Effect', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "effects" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({},{collection:'effects'});
    });
});