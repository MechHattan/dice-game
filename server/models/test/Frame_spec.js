var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Frame', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Frame = proxyQuire('../Frame', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "frames" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({},{collection:'frames'});
    });
});