var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Module', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Module = proxyQuire('../Module', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "modules" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({},{collection:'modules'});
    });
});