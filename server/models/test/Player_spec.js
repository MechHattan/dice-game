var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Player', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Player = proxyQuire('../Player', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "players" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({clean: jasmine.any(Function)},{collection:'players'});
    });
});