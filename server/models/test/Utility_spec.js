var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Utility', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Utility = proxyQuire('../Utility', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "utilities" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({},{collection:'utilities'});
    });
});