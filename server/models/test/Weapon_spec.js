var Model = require('../../Model'),
    mock = require('../../test-helpers/mock'),
    proxyQuire = require('proxyquire');

describe('Weapon', function() {
    var mockModel;

    beforeEach(function() {
        mockModel = mock(Model);
        var Weapon = proxyQuire('../Weapon', {
            '../Model': mockModel
        });
    });

    it('should extend Model', function() {
        expect(mockModel.extend).toHaveBeenCalled();
    });

    it('should pass in the "weapons" collection', function() {
        expect(mockModel.extend).toHaveBeenCalledWith({},{collection:'weapons'});
    });
});