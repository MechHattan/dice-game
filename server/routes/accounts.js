var accountService = require('../services/accounts');

module.exports = function(app) {
    app.post('/service/accounts/signin', function (req, res) {
        var playerData = {
            loginName: req.body.loginName,
            password: req.body.password
        };

        var player = accountService.signin(playerData);
        if(player) {
            req.session.username = player.username;
            res.json(player);
        } else {
            res.send(401);
        }

    });

    app.post('/service/accounts/signup', function (req, res) {
        var playerData = {
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        };
        var player = accountService.signup(playerData);
        req.session.username = player.username;
        res.json(player);
    });

    app.get('/service/accounts/current', function (req, res) {
        accountService.current(req, res);
    });

    app.post('/service/accounts/signout', function(req, res) {
        accountService.signout(req, res);
    });
};
