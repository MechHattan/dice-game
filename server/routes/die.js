var rollDie = require('../services/die');

module.exports = function(app) {
    app.get('/service/die/:sides/:rolls', function (req, res) {
        res.json({results: rollDie(req.params.sides, req.params.rolls)});
    });

    app.get('/service/die/:sides', function (req, res) {
        res.json({results: rollDie(req.params.sides, 1)});
    });
};