var messageService = require('../services/message');

module.exports = function (app) {
    app.post('/service/lobbies/main', function (req, res) {
        if(!req.session.color) {
            req.session.color = randomColor();
        }

        if(req.body && req.body.text) {
            messageService.post({
                color: req.session.color,
                type: 'chat',
                lobby: 'main',
                text: req.body.text,
                username: req.session.username || 'Guest'
            });
        }
        res.end();
    });
};

var hex = [0,1,2,3,4,5,6,7,8,9,'A','B','C','D','E','F'];

function randomColor() {
    var color = '';
    for(var i = 0; i < 6; i++) {
        color += hex[Math.floor(Math.random() * hex.length)];
    }
    return color;
}