var messageService = require('../services/message');

module.exports = function(app) {
    app.get('/service/messages', function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/event-stream'});
        res.write('retry: 10000\n');
        messageService.register(req, res);
    });
};