var mock = require('../../test-helpers/mock'),
    accountsService = require('../../services/accounts'),
    proxyQuire = require('proxyquire');

var instaniateAccountsRoutes = function(mockApp, mockAccountsService) {
    proxyQuire('../accounts', {
        '../services/accounts': mockAccountsService
    })(mockApp);
};

describe('accounts', function () {

    var callbacks,
        mockApp,
        mockAccountsService,
        mockReq,
        mockRes;

    beforeEach(function() {
        callbacks = {};
        mockApp = mock({
            post:function(){},
            get:function(){}
        });
        mockReq = jasmine.createSpy('req');
        mockRes =jasmine.createSpy('res');

        mockAccountsService = mock(accountsService);

        mockApp.post.andCallFake(function(url,callback) {
            callbacks[url] = callback;
        });
        mockApp.get.andCallFake(function(url,callback){
            callbacks[url] = callback;
        });

        instaniateAccountsRoutes(mockApp, mockAccountsService);
    });

    it('should provide a post method for /service/accounts/signin', function() {
        expect(mockApp.post).toHaveBeenCalledWith('/service/accounts/signin', jasmine.any(Function));
    });

    it('should provide a post method for /service/accounts/signup', function() {
        expect(mockApp.post).toHaveBeenCalledWith('/service/accounts/signup', jasmine.any(Function));
    });

    it('should provide a post method for /service/accounts/signout', function() {
        expect(mockApp.post).toHaveBeenCalledWith('/service/accounts/signout', jasmine.any(Function));
    });

    it('should provide a get method for /service/accounts/current', function() {
        expect(mockApp.get).toHaveBeenCalledWith('/service/accounts/current', jasmine.any(Function));
    });

    describe('for route /service/accounts/signin', function() {
        var url = '/service/accounts/signin';
        beforeEach(function() {
            mockReq = {
                body: {
                    loginName: 'login',
                    password: 'password'
                },
                session: {}
            };
            mockRes = {
                json: jasmine.createSpy('res json'),
                send: jasmine.createSpy('res send')
            };
        });
        it('should call accounts service to signin with data from req', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockAccountsService.signin.mostRecentCall.args[0]).toEqual({loginName: 'login', password: 'password'});
        });

        it('if player is found then set the session to the players username', function() {
            var dummyPlayer = {username: 'user'};
            mockAccountsService.signin.andReturn(dummyPlayer);
            callbacks[url](mockReq, mockRes);
            expect(mockReq.session.username).toBe(dummyPlayer.username);
        });

        it('if the player is found then set the response with the player', function() {
            var dummyPlayer = {};
            mockAccountsService.signin.andReturn(dummyPlayer);
            callbacks[url](mockReq, mockRes);
            expect(mockRes.json).toHaveBeenCalledWith(dummyPlayer);
        });

        it('if no player is found then set the response with 401', function() {
            mockAccountsService.signin.andReturn(undefined);
            callbacks[url](mockReq, mockRes);
            expect(mockRes.send).toHaveBeenCalledWith(401);
        });
    });

    describe('for route /service/accounts/signup', function() {
        var url = '/service/accounts/signup';
        var player = {username: 'username'};
        beforeEach(function() {
            mockRes.json = jasmine.createSpy();
            mockReq = {
                body: {
                    username: 'username',
                    firstName: 'firstName',
                    lastName: 'lastName',
                    email: 'email',
                    password: 'password'
                },
                session: {}
            };
            mockAccountsService.signup.andCallFake(function() {
               return player;
            });
            callbacks[url](mockReq, mockRes);
        });

        it('should call accounts service to signup', function() {
            expect(mockAccountsService.signup.mostRecentCall.args[0]).toEqual({
                username: 'username',
                firstName: 'firstName',
                lastName: 'lastName',
                email: 'email',
                password: 'password'
            });
        });

        it('should save username on request\'s session username', function() {
            expect(mockReq.session.username).toBe('username');
        });

        it('should set player on response', function() {
            expect(mockRes.json).toHaveBeenCalledWith(player);
        });
    });

    describe('for route /service/accounts/current', function() {
        var url = '/service/accounts/current';

        it('should call accounts service to current', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockAccountsService.current).toHaveBeenCalledWith(mockReq, mockRes);
        });
    });

    describe('for route /service/accounts/signout', function() {
        var url = '/service/accounts/signout';

        it('should call accounts service to signout', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockAccountsService.signout).toHaveBeenCalledWith(mockReq, mockRes);
        });
    });
});