var mock = require('../../test-helpers/mock'),
    dieService = require('../../services/die'),
    proxyQuire = require('proxyquire');

var instaniateDieRoutes = function(mockApp, mockDieService) {
    proxyQuire('../die', {
        '../services/die': mockDieService
    })(mockApp);
};

describe('die', function () {

    var callbacks,
        mockApp,
        mockDieService,
        resultFromDieService,
        mockReq,
        mockRes;

    beforeEach(function() {
        callbacks = {};
        mockApp = mock({
            get:function(){}
        });
        mockReq = {params: jasmine.createSpy('req')};
        mockRes = {json: jasmine.createSpy('res')};
        resultFromDieService = jasmine.createSpy('resultFromDieService');

        mockDieService = mock(dieService).andCallFake(function(sides, rolls) {
            return resultFromDieService;
        });

        mockApp.get.andCallFake(function(url,callback){
            callbacks[url] = callback;
        });

        instaniateDieRoutes(mockApp, mockDieService);
    });

    it('should provide a get method for /service/die/:sides/:rolls', function() {
        expect(mockApp.get).toHaveBeenCalledWith('/service/die/:sides/:rolls', jasmine.any(Function));
    });

    it('should provide a get method for /service/die/:sides', function() {
        expect(mockApp.get).toHaveBeenCalledWith('/service/die/:sides', jasmine.any(Function));
    });

    describe('for route /service/die/:sides/:rolls', function() {
        var url = '/service/die/:sides/:rolls';

        it('will call die service to roll', function() {
            mockReq.params.sides = 6;
            mockReq.params.rolls = 3;

            callbacks[url](mockReq, mockRes);
            expect(mockDieService).toHaveBeenCalledWith(6, 3);
        });

        it('returns value stored in results from die service', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockRes.json.mostRecentCall.args[0]).toEqual({results: resultFromDieService});
        });
    });

    describe('for route /service/die/:sides', function() {
        var url = '/service/die/:sides';

        it('will call die service to roll once', function() {
            mockReq.params.sides = 6;
            callbacks[url](mockReq, mockRes);
            expect(mockDieService).toHaveBeenCalledWith(6, 1);
        });

        it('returns value stored in results from die service', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockRes.json.mostRecentCall.args[0]).toEqual({results: resultFromDieService});
        });
    });
});