var mock = require('../../test-helpers/mock'),
    effectsModel = require('../../models/Effect'),
    proxyQuire = require('proxyquire');

var instaniateEffectsRoutes = function(mockApp, mockEffectsModel) {
    proxyQuire('../effects', {
        '../models/Effect': mockEffectsModel
    })(mockApp);
};

describe('effects', function () {

    var mockApp,
        mockEffectsModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockEffectsModel = mock(effectsModel);
        instaniateEffectsRoutes(mockApp, mockEffectsModel);
    });

    it('should servicify effects Model', function() {
        expect(mockEffectsModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});