var mock = require('../../test-helpers/mock'),
    frameModel = require('../../models/Frame'),
    proxyQuire = require('proxyquire');

var instaniateFrameRoutes = function(mockApp, mockFrameModel) {
    proxyQuire('../frames', {
        '../models/Frame': mockFrameModel
    })(mockApp);
};

describe('frames', function () {

    var mockApp,
        mockFrameModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockFrameModel = mock(frameModel);
        instaniateFrameRoutes(mockApp, mockFrameModel);
    });

    it('should servicify Frame Model', function() {
        expect(mockFrameModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});