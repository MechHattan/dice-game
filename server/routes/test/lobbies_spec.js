var mock = require('../../test-helpers/mock'),
    messageService = require('../../services/message'),
    proxyQuire = require('proxyquire');

var instaniateLobbiesRoutes = function(mockApp, mockMessageService) {
    proxyQuire('../lobbies', {
        '../services/message': mockMessageService
    })(mockApp);
};

describe('lobbies', function () {

    var callbacks,
        mockApp,
        mockMessageService,
        mockReq,
        mockRes;

    beforeEach(function() {
        callbacks = {};
        mockApp = mock({
            post:function(){}
        });
        mockMessageService = mock(messageService);
        mockReq = mock({session:{},body:{}});
        mockRes = mock({end:function(){}});

        mockApp.post.andCallFake(function(url,callback) {
           callbacks[url] = callback;
        });

        instaniateLobbiesRoutes(mockApp, mockMessageService);
    });

    it('should provide a post method for /service/lobbies/main', function() {
        expect(mockApp.post).toHaveBeenCalledWith('/service/lobbies/main', jasmine.any(Function));
    });

    describe('/service/lobbies/main', function() {

        var url = '/service/lobbies/main';

        it('should not post if no text is present', function() {
            callbacks[url](mockReq, mockRes);

            expect(mockMessageService.post).not.toHaveBeenCalled();
            expect(mockRes.end).toHaveBeenCalled();
        });

        it('should post the text to messageService if provided', function() {
            mockReq.body.text = 'hi';

            callbacks[url](mockReq, mockRes);

            expect(mockMessageService.post.mostRecentCall.args[0].text).toBe(mockReq.body.text);
        });

        it('should assign a different color to each session and pass it as part of the post', function () {
            mockReq.body.text = 'hi';

            callbacks[url](mockReq, mockRes);

            var color1 = mockReq.session.color;

            expectColor(mockMessageService.post.mostRecentCall.args[0].color);
            expect(color1).toBe(mockMessageService.post.mostRecentCall.args[0].color);

            delete mockReq.session.color;

            callbacks[url](mockReq, mockRes);

            var color2 = mockReq.session.color;

            expectColor(mockMessageService.post.mostRecentCall.args[0].color);
            expect(color2).toBe(mockMessageService.post.mostRecentCall.args[0].color);

            expect(color1).not.toBe(color2);
        });
    });
});

function expectColor(color) {
    var hex = '0123456789ABCDEF';
    expect(color).toBeDefined();
    expect(typeof color).toBe('string');
    expect(color.length).toBe(6);
    for(var i = 0; i < color.length; i++) {
        expect(hex.indexOf(color[i]) > -1).toBe(true);
    }
}