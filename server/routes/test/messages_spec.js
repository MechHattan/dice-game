var mock = require('../../test-helpers/mock'),
    messagesService = require('../../services/message'),
    proxyQuire = require('proxyquire');

var instaniateMessagesRoutes = function(mockApp, mockMessagesService) {
    proxyQuire('../messages', {
        '../services/message': mockMessagesService
    })(mockApp);
};

describe('messages', function () {

    var callbacks,
        mockApp,
        mockReq,
        mockRes,
        mockMessagesService;

    beforeEach(function() {
        callbacks = {};
        mockApp = mock({
            get:function(){}
        });

        mockReq = jasmine.createSpy('req');
        mockRes = {writeHead: jasmine.createSpy('res.writeHead'),
                   write: jasmine.createSpy('res.write')};

        mockMessagesService = mock(messagesService);

        mockApp.get.andCallFake(function(url,callback){
            callbacks[url] = callback;
        });

        instaniateMessagesRoutes(mockApp, mockMessagesService);
    });

    it('provides a get method for /service/messages', function() {
        expect(mockApp.get).toHaveBeenCalledWith('/service/messages', jasmine.any(Function));
    });

    describe('for route /service/messages', function() {
        var url = '/service/messages';

        it('will register with message service', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockMessagesService.register).toHaveBeenCalledWith(mockReq, mockRes);
        });

        it('writes to head an event stream', function() {
            callbacks[url](mockReq, mockRes);

            expect(mockRes.writeHead.mostRecentCall.args[0]).toEqual(200);
            expect(mockRes.writeHead.mostRecentCall.args[1]).toEqual({'Content-Type': 'text/event-stream'});
        });

        it('writes to head an retry for checking event stream', function() {
            callbacks[url](mockReq, mockRes);
            expect(mockRes.write).toHaveBeenCalledWith('retry: 10000\n');
        });
    });
});