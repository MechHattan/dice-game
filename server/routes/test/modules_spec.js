var mock = require('../../test-helpers/mock'),
    moduleModel = require('../../models/Module'),
    proxyQuire = require('proxyquire');

var instaniateModuleRoutes = function(mockApp, mockModuleModel) {
    proxyQuire('../modules', {
        '../models/Module': mockModuleModel
    })(mockApp);
};

describe('modules', function () {

    var mockApp,
        mockModuleModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockModuleModel = mock(moduleModel);
        instaniateModuleRoutes(mockApp, mockModuleModel);
    });

    it('should servicify Module Model', function() {
        expect(mockModuleModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});