var mock = require('../../test-helpers/mock'),
    playerModel = require('../../models/Player'),
    proxyQuire = require('proxyquire');

var instaniatePlayersRoutes = function(mockApp, mockPlayerModel) {
    proxyQuire('../players', {
        '../models/Player': mockPlayerModel
    })(mockApp);
};

describe('players', function () {

    var mockApp,
        mockPlayerModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockPlayerModel = mock(playerModel);
        instaniatePlayersRoutes(mockApp, mockPlayerModel);
    });

    it('should servicify Player Model', function() {
        expect(mockPlayerModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});