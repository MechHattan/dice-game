var mock = require('../../test-helpers/mock'),
    utilityModel = require('../../models/Utility'),
    proxyQuire = require('proxyquire');

var instaniateUtilitiesRoutes = function(mockApp, mockUtilityModel) {
    proxyQuire('../utilities', {
        '../models/Utility': mockUtilityModel
    })(mockApp);
};

describe('utilities', function () {

    var mockApp,
        mockUtilityModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockUtilityModel = mock(utilityModel);
        instaniateUtilitiesRoutes(mockApp, mockUtilityModel);
    });

    it('should servicify Utility Model', function() {
        expect(mockUtilityModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});