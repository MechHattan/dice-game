var mock = require('../../test-helpers/mock'),
    weaponModel = require('../../models/Weapon'),
    proxyQuire = require('proxyquire');

var instaniateWeaponsRoutes = function(mockApp, mockWeaponModel) {
    proxyQuire('../weapons', {
        '../models/Weapon': mockWeaponModel
    })(mockApp);
};

describe('weapons', function () {

    var mockApp,
        mockWeaponModel;

    beforeEach(function() {
        mockApp = jasmine.createSpy('App');
        mockWeaponModel = mock(weaponModel);
        instaniateWeaponsRoutes(mockApp, mockWeaponModel);
    });

    it('should servicify Weapon Model', function() {
        expect(mockWeaponModel.servicify).toHaveBeenCalledWith(mockApp);
    });
});