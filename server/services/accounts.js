var Player = require('../models/Player'),
    hash = require('../util/hash');

module.exports = {
    signin: function(playerData) {
        var passhash = hash(playerData.password);
        var player = Player.findOne({username: playerData.loginName, passhash: passhash});
        if(player === undefined) {
            player = Player.findOne({email: playerData.loginName, passhash: passhash});
        }

        if(player) {
            return player.clean();
        }
        return undefined;
    },

    signup: function(playerData) {
        var player = new Player({
            username: playerData.username,
            firstName: playerData.firstName,
            lastName: playerData.lastName,
            email: playerData.email,
            passhash: hash(playerData.password)
        });
        player.save();
        return player.clean();
    },

    current: function(req, res) {
        if (req.session) {
            var player = Player.findOne({username: req.session.username});
            if (player) {
                res.json(player.clean());
            } else {
                res.send(401);
            }
        } else {
            res.send(401);
        }
    },

    signout: function(req, res) {
        req.session.destroy();
        res.send(200);
    }
};