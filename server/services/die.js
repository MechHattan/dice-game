module.exports = function (sides, rolls) {
    var results = [];
    rolls = rolls || 1;
    for (var i = 0; i < rolls; i++) {
        results.push(Math.floor(Math.random() * sides) + 1);
    }
    return results;
};