var _ = require('underscore');

var byUsername = {
};

var byId = {
};

function write(res, event) {
    res.write('data: ' + JSON.stringify(event) + '\n\n');
}

module.exports = {
    post: function (event) {
        var res;
        Object.keys(byId).forEach(function (id) {
            res = byId[id];
            if(res.finished) {
                console.log(id + ' disconnected from the server.');
                delete byId[id];
            } else {
                write(res, event);
            }
        });
    },
    postByUsername: function (usernames, event) {
        var res;
        usernames.forEach(function (username) {
            res = byUsername[username];
            if (res) {
                if (res.finished) {
                    console.log(username + ' disconnected from the server.');
                    delete byUsername[username];
                } else {
                    write(res, event);
                }
            }
        });
    },
    register: function (req, res) {
        res.on('close', function() {
            this.finished = true;
        });
        if (req.session.username) {
            console.log(req.session.username + ' connected to the server.');
            byUsername[req.session.username] = res;
        }
        console.log(req.sessionID + ' connected to the server.');
        byId[req.sessionID] = res;
    }
};