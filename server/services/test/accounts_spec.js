var mock = require('../../test-helpers/mock'),
    PlayerModel = require('../../models/Player'),
    hash = require('../../util/hash'),
    proxyQuire = require('proxyquire');

var instaniateAccountsService = function(mockPlayerModel, mockHash) {
    return proxyQuire('../accounts', {
        '../models/Player': mockPlayerModel,
        '../util/hash': mockHash
    });
};

var playerData = {
        loginName: 'loginName',
        username: 'Gwendols',
        firstName: 'Gwen',
        lastName: 'Kubacki',
        email: 'Gwen.Kubacki@email.com',
        password: 'I_LOVE_ERIC'
};

describe('accounts', function () {
    var accountsService,
        mockPlayerModel,
        mockHash,
        passhash = 'password';

    beforeEach(function() {

        mockPlayerModel = mock(PlayerModel);
        mockHash = mock(hash);
        mockHash.andCallFake(function() {
            return passhash;
        });

        accountsService = instaniateAccountsService(mockPlayerModel, mockHash);
    });

    describe('signup', function() {
        beforeEach(function() {
            accountsService.signup(playerData);
        });

        describe('should create player instance from passed in', function() {
            it('firstName', function() {
                expect(mockPlayerModel.mostRecentCall.args[0].firstName).toBe(playerData.firstName);
            });
            it('lastName', function() {
                expect(mockPlayerModel.mostRecentCall.args[0].lastName).toBe(playerData.lastName);
            });
            it('userName', function() {
                expect(mockPlayerModel.mostRecentCall.args[0].username).toBe(playerData.username);
            });
            it('email', function() {
                expect(mockPlayerModel.mostRecentCall.args[0].email).toBe(playerData.email);
            });
            it('password and create hash of it', function() {
                expect(mockHash).toHaveBeenCalledWith(playerData.password);
                expect(mockPlayerModel.mostRecentCall.args[0].passhash).toBe(passhash);
            });
        });

        it('should save created player instance', function() {
            expect(mockPlayerModel.prototype.save).toHaveBeenCalled();
        });

        it('should return public version of player instance', function() {
            var expectedPlayer = jasmine.createSpy();
            mockPlayerModel.prototype.clean.andCallFake(function() {
                return expectedPlayer;
            });

            var actualPlayer = accountsService.signup(playerData);
            expect(mockPlayerModel.prototype.clean).toHaveBeenCalled();
            expect(actualPlayer).toBe(expectedPlayer);
        });
    });

    describe('signin', function() {
        beforeEach(function() {
            mockPlayerModel.findOne.andReturn(new mockPlayerModel({}));
        });
        it('should create passhass from password', function() {
            accountsService.signin(playerData);
            expect(mockHash).toHaveBeenCalledWith(playerData.password);
        });

        it('should look for player by username', function() {
            accountsService.signin(playerData);
            expect(mockPlayerModel.findOne.mostRecentCall.args[0]).toEqual({username:playerData.loginName, passhash: passhash});
        });

        it('should look for player by email if no username is available', function() {
            var calls = 0;
            mockPlayerModel.findOne.andCallFake(function(arg) {
                if(calls) {
                    return new mockPlayerModel();
                }
                calls++;
                return undefined;
            });

            accountsService.signin(playerData);
            expect(mockPlayerModel.findOne.mostRecentCall.args[0]).toEqual({email:playerData.loginName, passhash: passhash});
        });

        it('should return publicized player if found', function() {
            var expectedPlayer = jasmine.createSpy();
            mockPlayerModel.prototype.clean.andCallFake(function() {
                return expectedPlayer;
            });

            var actualPlayer = accountsService.signin(playerData);
            expect(mockPlayerModel.prototype.clean).toHaveBeenCalled();
            expect(actualPlayer).toBe(expectedPlayer);
        });

        it('should return undefined if no player is ever found', function() {
            mockPlayerModel.findOne.andReturn(undefined);
            expect(accountsService.signin(playerData)).not.toBeDefined();
        });
    });
});
