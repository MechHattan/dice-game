module.exports = function(obj) {
    if (typeof obj === 'function') {
        return mockFunction(obj);
    }
    return mockObject(obj);
};

function mockFunction(obj) {
    var spyFn = jasmine.createSpy(obj.name);
    Object.keys(obj).forEach(function(key) {
        spyFn[key] = jasmine.createSpy(obj, key);
    });
    spyFn.prototype = mockObject(obj.prototype);
    return spyFn;
}

function mockObject(obj) {
    var keys = Object.keys(obj);
    if (keys.length) {
        return jasmine.createSpyObj('spy', keys);
    }
    return obj;
}