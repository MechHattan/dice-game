var _ = require('underscore');

var extend = module.exports = function (props, staticProps) {
    var child, parent;

    if (this.extend) {
        parent = this;
    }

    if (props && _.has(props, 'constructor')) {
        child = props.constructor;
    } else if (parent) {
        child = function () {
            return parent.apply(this, arguments);
        };
    } else {
        child = function () {
        };
    }

    child.extend = extend;

    _.extend(child.prototype, parent ? parent.prototype : {}, props);
    _.extend(child, parent, staticProps);

    return child;
};