var crypto = require('crypto');

module.exports = function(content) {
    return crypto.createHash('sha1').update(content).digest().toString();
};